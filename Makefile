run: test
	./test
	./test foo 123 | grep 'Foo: 123'
	./test bar | grep 'bar: TRUE'
	./test foo | grep -e 'Foo: 0,'

test: test.c main.c
	cc -o test test.c main.c

main.c: test.c automain.sh genmain.sh Makefile
	./automain.sh > main.c

