#!/bin/sh
# This script looks through your code-base for a funtion named "int
# run (...)" and uses that as a template for automatically generating
# a "int main (int argc, char **argv)" function which parses command
# line arguments and lines them up with the arguments received by run

echo '#include "automain.h"'
echo ''
echo 'int parse_int(const char *src) {'
echo '    long val;'
echo '    val = strtol(src, NULL, 10);'
echo '    return (int) val;'
echo '}'
echo ''
grep -he '^int run *(' *.c | sed -e 's/ *{.*/;/'
echo ''
echo 'int main(const int argc, const char **argv) {'
echo '    int argidx;'
grep -e '^int run *(' `ls *.c | grep -v main.c` | sed -e 's/.*(//' -e 's/).*//' -e 's/ *, */,/' | tr , \\n | sed -e 's/^/"/' -e 's/$/"/' | xargs ./genmain.sh
