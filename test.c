#include <stdio.h>
#include "automain.h"

int run(int foo, bool bar) {
    printf("Foo: %d, bar: %s\n", foo, (bar) ? "TRUE" : "FALSE");
    return 0;
}
