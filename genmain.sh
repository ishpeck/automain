#!/bin/sh

# Called by automain.sh
passin=""
preface=""
searches=""
searchsep="       "
while `test -n "$1"`; do
    argtype=`echo $1 | awk '{print $1}'`
    argname=`echo $1 | awk '{print $2}'`
    argdefault="0"
    passin="$passin$preface$argname"
    preface=", "
    case $argtype in
        bool)
            searches="$searches$searchsep if(strcmp(argv[argidx], \"$argname\")==0) { $argname=TRUE; }"
            argdefault="FALSE"
            ;;
        *)
            searches="$searches$searchsep if(strcmp(argv[argidx], \"$argname\")==0 && argidx<(argc-1)) { $argname=parse_$argtype(argv[argidx+1]); }"
            ;;
    esac
    searchsep="\n       "
    echo "    $1=$argdefault;"
    shift
done
echo '    for (argidx=1;argidx<argc;++argidx) {'
echo "$searches"
echo '    }'
echo "    return run($passin);"
echo '}'
